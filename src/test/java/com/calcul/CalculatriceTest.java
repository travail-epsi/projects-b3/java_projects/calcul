package com.calcul;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatriceTest {
  @Test
  public void testAddition() {
    int a = 15, b = 5;
    Calculatrice instance = new Calculatrice();
    int expResult = 20;
    int result = instance.addition(a, b);
    assertEquals(expResult, result);
  }

  @Test
  public void testSoustraction() {
    int a = 15, b = 5;
    Calculatrice instance = new Calculatrice();
    int expResult = 10;
    int result = instance.soustraction(a, b);
    assertEquals(expResult, result);
  }

  @Test
  public void testDivision() throws Exception {
    int a = 15, b = 5;
    Calculatrice instance = new Calculatrice();
    int expResult = 3;
    int result = 0;
    result = instance.division(a, b);
    assertEquals(expResult, result);
  }

  @Test(expected = Exception.class)
  public void testDivisionPar0() throws Exception {
    int a = 15, b = 0;
    Calculatrice instance = new Calculatrice();
    int result = instance.division(a, b);
  }

  @Test
  public void testMultiplication() {
    int a = 15, b = 5;
    Calculatrice instance = new Calculatrice();
    int expResult = 75;
    int result = instance.multiplication(a, b);
    assertEquals(expResult, result);
  }

}
