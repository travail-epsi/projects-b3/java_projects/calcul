package com.calcul;

public class Calculatrice {
  public int addition(int a, int b) {
    return a + b;
  }

  public int soustraction(int a, int b) {
    return a - b;
  }

  public int multiplication(int a, int b) {
    return a * b;
  }

  public int division(int a, int b) throws Exception{
  if (b == 0) {
    throw new MonException();
  }
  return a / b;
  
  //   int result = 0;

  //   try {
  //     result = a / b;
  //   } catch (Exception e) {
  //     System.err.println(e.getMessage());
  //   }
  //   return result;

  // }

  // public double division(Double a, Double b) {
  //   return a / b;
  }
}
