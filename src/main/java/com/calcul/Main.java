package com.calcul;

public class Main {
  public static void main(String[] args) {
    Calculatrice c = new Calculatrice();

    int a = 15, b = 15;
    System.out.println(c.addition(a, b));
    System.out.println(c.soustraction(a, b));
    System.out.println(c.multiplication(a, b));
    try {
      System.out.println(c.division(10, 0));
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
    try {
      System.out.println(c.division(0, 10));
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }
}
