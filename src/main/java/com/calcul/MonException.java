package com.calcul;


public class MonException extends Exception{
  public MonException() {
    super("Division par zéro impossible");
  }
}
